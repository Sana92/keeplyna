<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;

/*
* Nom de classe : UserService
*
* Description   : service contenant toutes les fonction liés
*                 à la gestion des comptes utilistaurs.
*
* Version       : 1.0
*
* Date          : 26/07/2019
*
* Copyright     : SANA
**/
class UserService
{
    const C_KEY_ARRAY_USERNAME         = 'username';


    public function __construct(
        EntityManagerInterface $i__entityManager,
        ContainerInterface $i__container)
    {
        $this->em        = $i__entityManager;
        $this->container = $i__container;
    }


    /**
    * Checks user credentials
    */
    public function toCheckCredentials(
        $i__encoder,
        $i__username,
        $i__password
        )
    {
        $l__user = $this->em->getRepository(User::class)
                ->findOneBy(array(UserService::C_KEY_ARRAY_USERNAME=> $i__username));

        // Check if the user exists !
        if (!$l__user) {
            return 'Invalid User';
        }
        else if (!$i__encoder->isPasswordValid($l__user, $i__password)) {
            return 'Invalid Password';
        } else {
            return $l__user;
        }
    }


    /**
     * Generates a JsonWebToken from user.
     */
    public function toGenerateToken($i__user)
    {
        // Call the jwt_manager service & create the token
        return $this->container->get('lexik_jwt_authentication.jwt_manager')->create($i__user);
    }

    /**
     * Get role of user.
     */
    public function toGetRole($i__user)
    {
        $l__userId = $i__user->getId();
        $l__user = $this->em->getRepository(User::class)
                ->find($l__userId);
        return $l__user->getRoles();
    }



}
