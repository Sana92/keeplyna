<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/*
* Nom de classe : UserService
*
* Description   : service contenant toutes les fonction liés aux abonnées.
*
* Version       : 1.0
*
* Date          : 31/07/2019
*
* Copyright     : SANA
**/
class SubscriberService
{


    public function __construct(
        EntityManagerInterface $i__entityManager,
        ContainerInterface $i__container)
    {
        $this->em        = $i__entityManager;
        $this->container = $i__container;
    }


     public function toGetSubscriberList($i__logger,$i__authenticatedUsername)
     {
      $i__logger->addInfo('Entering method toGetSubscriberList() in SubscriberService');
      try {
          $l__dbConnection = $this->container->get('doctrine.dbal.customer_connection');
          $l__subscriberItmes = array();
          $l__users = $l__dbConnection->fetchAll('SELECT * FROM subscriber');
          foreach($l__users as $value )
          {
          $l__subscriberItmes[] = array( "id"        => $value["id"],
                                         "firstname" => $value["firstname"],
                                         "lastname"  => $value["lastname"],
                                         "active"    => $value["active"] );
          }
          $i__logger->addInfo('The user '.$i__authenticatedUsername.': List of subscribers got SUCCESSFULLY');

      } catch (\Exception $e) {
          throw new \Exception('Failed to get list of subscribers'.$e->getMessage());
          $i__logger->addError('Failed to get list of subscribers'.$e->getMessage());
      }

      $i__logger ->addInfo('End method toGetSubscriberList() in SubscriberService');
      return $l__subscriberItmes;
     }


     public function toActivatetDesactivateListSubscribers($i__logger,$i__authenticatedUsername,$i__data)
     {
       $i__logger->addInfo('Entering method toActivatetDesactivateListSubscribers() in SubscriberService');
       $l__dbConnection = $this->container->get('doctrine.dbal.customer_connection');

        try {
            for ($i=0; $i <count($i__data) ; $i++) {
              if ($i__data[$i]['status']) {
                $l__status  = 1;
              } else {
                $l__status  = 0;
              }
              $l__query   = 'UPDATE subscriber SET active = (?)  where id = (?)';
              $l__result  = $l__dbConnection->executeQuery($l__query,array($l__status,$i__data[$i]['id']));
            }

            $i__logger->addInfo('The user '.$i__authenticatedUsername.': subscribers are activated/desactivated SUCCESSFULLY');

        } catch (\Exception $e) {
            throw new \Exception('Failed to activate/desactivate subscribers'.$e->getMessage());
            $i__logger->addError('Failed to activate/desactivate subscribers'.$e->getMessage());
        }

        $i__logger ->addInfo('End method toActivatetDesactivateListSubscribers() in SubscriberService');
        return array('result' => 'Success');
     }


     public function toActivatetDesactivateSubscriber($i__logger,$i__authenticatedUsername,$i__data)
     {
       $i__logger->addInfo('Entering method toActivatetDesactivateSubscriber() in SubscriberService');
       $l__dbConnection = $this->container->get('doctrine.dbal.customer_connection');

        try {
          if ($i__data['status']) {
          $l__status  = 1;
          } else {
          $l__status  = 0;
          }
          $l__query   = 'UPDATE subscriber SET active = (?)  where id = (?)';
          $l__result  = $l__dbConnection->executeQuery($l__query,array($l__status,$i__data['id']));

          $i__logger->addInfo('The user '.$i__authenticatedUsername.': subscriber is activated/desactivated SUCCESSFULLY');

        } catch (\Exception $e) {
            throw new \Exception('Failed to activate/desactivate subscriber'.$e->getMessage());
            $i__logger->addError('Failed to activate/desactivate subscriber'.$e->getMessage());
        }

        $i__logger ->addInfo('End method toActivatetDesactivateSubscriber() in SubscriberService');
        return array('result' => 'Success');
     }

     public function toGetActivatedSubscriber($i__logger,$i__authenticatedUsername)
     {
       $i__logger->addInfo('Entering method toGetActivatedSubscriber() in SubscriberService');
       $l__dbConnection = $this->container->get('doctrine.dbal.customer_connection');

        try {

          $l__queryActivated   = 'SELECT count(*) AS  count FROM subscriber WHERE active =(?)';
          $l_resultActivated = $l__dbConnection->executeQuery($l__queryActivated,array(1))->fetchAll();
          $l__countActivated = $l_resultActivated[0]['count'];

          $l__queryDisabled  = 'SELECT count(*) AS count FROM subscriber WHERE active =(?)';
          $l_resultDisabled = $l__dbConnection->executeQuery($l__queryDisabled,array(0))->fetchAll();
          $l__countDisabled = $l_resultDisabled[0]['count'];


          $i__logger->addInfo('The user '.$i__authenticatedUsername.': activated/disabled subscribers are got SUCCESSFULLY');

        } catch (\Exception $e) {
            throw new \Exception('Failed to get activated/disabled subscribers'.$e->getMessage());
            $i__logger->addError('Failed to get activated/disabled subscribers'.$e->getMessage());
        }

        $i__logger ->addInfo('End method toGetActivatedSubscriber() in SubscriberService');
        return array('active'    => $l__countActivated,
                     'desactive' => $l__countDisabled);
     }

     public function toGetSubscriberDetails($i__logger,$i__authenticatedUsername,$l__subscriberId)
     {
      $i__logger->addInfo('Entering method toGetSubscriberDetails() in SubscriberService');
      try {
          $l__dbConnection = $this->container->get('doctrine.dbal.customer_connection');
          $l__result = array();
          $l__subscriber = $l__dbConnection->executeQuery('SELECT * FROM subscriber WHERE id =(?)',array($l__subscriberId))->fetchAll();
          foreach($l__subscriber as $value )
          {
          $l__result[] = array(  "id"        => $value["id"],
                                 "firstname" => $value["firstname"],
                                 "lastname"  => $value["lastname"],
                                 "active"    => $value["active"] ,
                                 "email"     => $value["email"]);
          }
          $i__logger->addInfo('The user '.$i__authenticatedUsername.': Details of subscribers got SUCCESSFULLY');

      } catch (\Exception $e) {
          throw new \Exception('Failed to get list of subscribers'.$e->getMessage());
          $i__logger->addError('Failed to get list of subscribers'.$e->getMessage());
      }

      $i__logger ->addInfo('End method toGetSubscriberDetails() in SubscriberService');
      return $l__result;
     }




}
