<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Table(name="tb_user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\AttributeOverrides({
 * @ORM\AttributeOverride(name="password",column=@ORM\Column(nullable=true)),
 * @ORM\AttributeOverride(name="email", column=@ORM\Column(type="string", name="email", length=255, unique=false, nullable=true)),
 * @ORM\AttributeOverride(name="emailCanonical", column=@ORM\Column(type="string", name="email_canonical", length=255, unique=false, nullable=true))
* })
 */
class User extends BaseUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
   * @var string
   *
   * @ORM\Column(name="role", type="string", length=255, unique=false, nullable=true)
   */

    public function getId(): ?int
    {
        return $this->id;
    }
}
