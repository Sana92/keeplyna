<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use App\Service\UserService;
use Psr\Log\LoggerInterface;


/*
* Nom de classe : LoginController
*
* Description   : Contrôleur contenant toutes les apis liés à
*                 la connexions Web et aux comptes utitisateurs.
*
* Version       : 1.0
*
* Date          : 26/07/2019
*
* Copyright     : SANA
**/

class LoginController extends AbstractController
{
    const C_USERNAME       = "username";
    const C_RESULT         = "result";
    const C_TOKEN_FAILURE  = 'Failed to get JWT Token :';

    private $logger;

    public function __construct(LoggerInterface $logger)
    {
      $this->logger = $logger;

    }

    /*
    * This function is used to authenticate the user on login
    */

    /**
    * @Route("/authenticate",name="authentication", methods={"POST"})
    */
    public function toLoginAction(
                                    Request $i__request ,
                                    UserService $i__userService,
                                    UserPasswordEncoderInterface $i__encoder)
    {

      try{
        $this->logger            ->addInfo('Entering method toLoginAction() in LoginController');
        /*
        * Get Request body content (username, password, device)
        */
        $l__username  = $i__request->get("username");
        $l__password  = $i__request->get("password");
         /*
        * Check the credentials of th user, this function returns :
        * 'Invalid User'     if username is false
        * 'Invalid Password' if password is false
        * User Object        if credentails are valid
        */
        $l__isValid   = $i__userService->toCheckCredentials(
            $i__encoder,
            $l__username,
            $l__password
            );

        if (($l__isValid == 'Invalid User') || ($l__isValid  == 'Invalid Password')) {
             //Bad Credentials
            if($l__isValid == 'Invalid User') {
                //Bad username
                $l__response         = array(LoginController::C_RESULT  => "username invalid");
                $this->logger  ->addInfo('CONNEXION FAILED : There is no user with this username ');

            } else {
                //Bad password
                $l__response         = array(LoginController::C_RESULT  => "password invalid");
                $this->logger  ->addInfo('CONNEXION FAILED : There is no user with this password ');

            }
        }else {
            //Correct credentials
            $l__user = $l__isValid;
            $l__authenticatedUserId = $l__user->getId();
            $l__authenticatedUsername = $l__user->getUsername();
            $this->logger ->addInfo('The user '.$l__authenticatedUsername.' is CONNECTED SUCCESSFULLY');
            /*
            * Generates the JWT token to send to the user
            */
            $l__token = $i__userService->toGenerateToken($l__user);
            $this->logger      ->addInfo('The user '.$l__authenticatedUsername.' : Generated Token');
            /*
            * Get role of user
            */
            $l__role = $i__userService->toGetRole($l__user);
            $l__response = array(LoginController::C_RESULT  => "Success" ,'token'  => $l__token , 'role'=>$l__role, "username"=>$l__authenticatedUsername);


        }
        }catch(Exception $e){
            throw new UnexpectedValueException(LoginController::C_TOKEN_FAILURE.$e->getMessage());
            $this->logger->addError('The user '.$l__authenticatedUserId.'Failed to connect : '.$e->getMessage());


        }
        $this->logger            ->addInfo('End method toLoginAction() in LoginController');

        return new JsonResponse($l__response);
    }
}
