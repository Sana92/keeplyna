<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\SubscriberService;
use Symfony\Component\HttpFoundation\Request;





/*
* Nom de classe : SubscriberController
*
* Description   : Contrôleur contenant toutes les apis liés aux abonnées.
*
* Version       : 1.0
*
* Date          : 31/07/2019
*
* Copyright     : SANA
**/
class SubscriberController extends AbstractController
{
  const C_SECURITY_TOKEN_STORAGE     = 'security.token_storage';
  const C_MESSAGE_FAILED_TOKEN       = "Failed to get JWT Token: ";

  private $logger;

  public function __construct(LoggerInterface $logger)
  {
    $this->logger = $logger;

  }
    /*
    * This function is used to get list of all subscribers
    */

    /**
     * @Route("/api/subscriber/list", name="subscriber_list",methods={"POST"})
     */
    public function toGetListAction(SubscriberService $i__subscriberService)
    {
      $this->logger ->addInfo('Entering method toGetListAction() in SubscriberController');
      /*
      * Get User Id from the token used in the request
      */
      try {
          $l__authenticatedUserId = $this->container->get(SubscriberController::C_SECURITY_TOKEN_STORAGE)
                                  ->getToken()->getUser()->getId();
          $l__authenticatedUsername = $this->container->get(SubscriberController::C_SECURITY_TOKEN_STORAGE)
                                  ->getToken()->getUser()->getUsername();
      } catch (\Exception $e) {
          $this->logger            ->addError(SubscriberController::C_MESSAGE_FAILED_TOKEN.$e->getMessage());
          throw new \Exception(SubscriberController::C_MESSAGE_FAILED_TOKEN.$e->getMessage());
      }

      $l__subscribers = $i__subscriberService->toGetSubscriberList($this->logger,$l__authenticatedUsername);

      $this->logger ->addInfo('End method toGetListAction() in SubscriberController');

      return new JsonResponse($l__subscribers);
    }
      /*
      * This function is used to activate or deactivate list of subscribers
      */

      /**
      * @Route("/api/subscriber/list/active", name="subscriber_list__active",methods={"POST"})
      */

      public function toActivatetDesactivateListAction( Request $i__request ,
                                         SubscriberService $i__subscriberService)
      {
        $this->logger ->addInfo('Entering method toActivatetDesactivateListAction() in SubscriberController');

        /*
        * Get User Id from the token used in the request
        */
        try {
            $l__authenticatedUserId = $this->container->get(SubscriberController::C_SECURITY_TOKEN_STORAGE)
                                    ->getToken()->getUser()->getId();
            $l__authenticatedUsername = $this->container->get(SubscriberController::C_SECURITY_TOKEN_STORAGE)
                                    ->getToken()->getUser()->getUsername();
        } catch (\Exception $e) {
            $this->logger            ->addError(SubscriberController::C_MESSAGE_FAILED_TOKEN.$e->getMessage());
            throw new \Exception(SubscriberController::C_MESSAGE_FAILED_TOKEN.$e->getMessage());
        }

        $this->logger->addInfo('the User'.$l__authenticatedUsername.': makes a request to activate/deactivate list of subscribers');
        /*
        * Get the the body's request
        */
        $l__data = json_decode($i__request->getContent(), true);
        $l__result = $i__subscriberService->toActivatetDesactivateListSubscribers($this->logger,$l__authenticatedUsername,$l__data);

        $this->logger ->addInfo('End method toActivatetDesactivateListAction() in SubscriberController');

        return new JsonResponse($l__result);
      }

      /*
      * This function is used to activate  or deactivate a subscriber
      */

      /**
      * @Route("/api/subscriber/active", name="subscriber_active",methods={"POST"})
      */

      public function toActivatetDesactivateAction( Request $i__request ,
                                         SubscriberService $i__subscriberService)
      {
        $this->logger ->addInfo('Entering method toActivatetDesactivateAction() in SubscriberController');

        /*
        * Get User Id from the token used in the request
        */
        try {
            $l__authenticatedUserId = $this->container->get(SubscriberController::C_SECURITY_TOKEN_STORAGE)
                                    ->getToken()->getUser()->getId();
            $l__authenticatedUsername = $this->container->get(SubscriberController::C_SECURITY_TOKEN_STORAGE)
                                    ->getToken()->getUser()->getUsername();
        } catch (\Exception $e) {
            $this->logger            ->addError(SubscriberController::C_MESSAGE_FAILED_TOKEN.$e->getMessage());
            throw new \Exception(SubscriberController::C_MESSAGE_FAILED_TOKEN.$e->getMessage());
        }

        $this->logger->addInfo('the User'.$l__authenticatedUsername.': makes a request to activate/deactivate a subscriber');
        /*
        * Get the the body's request
        */
        $l__data = json_decode($i__request->getContent(), true);

        $l__result = $i__subscriberService->toActivatetDesactivateSubscriber($this->logger,$l__authenticatedUsername,$l__data);

        $this->logger ->addInfo('End method toActivatetDesactivateAction() in SubscriberController');

        return new JsonResponse($l__result);
      }

      /*
      * This function is used to get the number of activated and disabled subscribers
      */

      /**
      * @Route("/api/subscriber/activated", name="subscriber_activated",methods={"POST"})
      */

      public function toGetActivatedAction(SubscriberService $i__subscriberService)
      {
        $this->logger ->addInfo('Entering method toGetActivatedAction() in SubscriberController');

        /*
        * Get User Id from the token used in the request
        */
        try {
            $l__authenticatedUserId = $this->container->get(SubscriberController::C_SECURITY_TOKEN_STORAGE)
                                    ->getToken()->getUser()->getId();
            $l__authenticatedUsername = $this->container->get(SubscriberController::C_SECURITY_TOKEN_STORAGE)
                                    ->getToken()->getUser()->getUsername();
        } catch (\Exception $e) {
            $this->logger            ->addError(SubscriberController::C_MESSAGE_FAILED_TOKEN.$e->getMessage());
            throw new \Exception(SubscriberController::C_MESSAGE_FAILED_TOKEN.$e->getMessage());
        }

        $l__result = $i__subscriberService->toGetActivatedSubscriber($this->logger,$l__authenticatedUsername);

        $this->logger ->addInfo('End method toGetActivatedAction() in SubscriberController');

        return new JsonResponse($l__result);
      }

      /*
      * This function is used  get details of the given subscriber
      */

      /**
       * @Route("/api/subscriber/details", name="subscriber_details",methods={"POST"})
       */
      public function toGetDetailsAction(Request $i__request ,SubscriberService $i__subscriberService)
      {
        $this->logger ->addInfo('Entering method toGetDetailsAction() in SubscriberController');
        /*
        * Get User Id from the token used in the request
        */
        try {
            $l__authenticatedUserId = $this->container->get(SubscriberController::C_SECURITY_TOKEN_STORAGE)
                                    ->getToken()->getUser()->getId();
            $l__authenticatedUsername = $this->container->get(SubscriberController::C_SECURITY_TOKEN_STORAGE)
                                    ->getToken()->getUser()->getUsername();
        } catch (\Exception $e) {
            $this->logger            ->addError(SubscriberController::C_MESSAGE_FAILED_TOKEN.$e->getMessage());
            throw new \Exception(SubscriberController::C_MESSAGE_FAILED_TOKEN.$e->getMessage());
        }
        /*
        * Get the the body's request
        */
        $l__data = json_decode($i__request->getContent(), true);
        $l__subscriberId = $l__data["id"];
        $this->logger->addInfo('the User'.$l__authenticatedUsername.': makes a request to get details of the subscriber with id :'.$l__subscriberId);


        $l__result = $i__subscriberService->toGetSubscriberDetails($this->logger,$l__authenticatedUsername,$l__subscriberId);

        $this->logger ->addInfo('End method toGetDetailsAction() in SubscriberController');

        return new JsonResponse($l__result);
      }
}
