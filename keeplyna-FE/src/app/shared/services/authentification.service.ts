import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Router } from '@angular/router';

import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {
  private _loginUrl = 'http://localhost:8000/authenticate';

  constructor(private http: HttpClient,private router: Router) { }

  loginUser(username: string, password: string): Observable<any> {
    const body = new HttpParams()
        .set('username', username)
        .set('password', password);

    return this.http.post(this._loginUrl,
        body.toString(),
        {
          headers: new HttpHeaders()
              .set('Content-Type', 'application/x-www-form-urlencoded')
        }
    );
  }

  public getToken(): string {
  return localStorage.getItem('token');
  }


  public getRole(): string {
  return localStorage.getItem('role');
  }

  public isLoggedIn(){
      if(localStorage.getItem('token') !== null){
          return true
      }else{
          this.router.navigate(['/login']);
          return false
      }
  }

  public logout(){
      localStorage.removeItem('token');
      localStorage.removeItem('role');
      localStorage.removeItem('isLoggedin');

  }
}
