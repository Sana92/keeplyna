import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthentificationService} from './authentification.service'
import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SubscribersService{
    private url = 'http://localhost:8000/api/subscriber/list';

    constructor(private http: HttpClient, private router: Router, private auth: AuthentificationService ) { }

    getsubscribers(): Observable<any>{
        var header = {
            headers: new HttpHeaders({'content-type': 'application/json'})
              .set('Authorization',  `Bearer ${this.auth.getToken()}`)                 
            } 
            var data ;
            return   this.http.post(this.url, data, header);

    }


    public getToken(): string {
        return localStorage.getItem('token');
        }



}