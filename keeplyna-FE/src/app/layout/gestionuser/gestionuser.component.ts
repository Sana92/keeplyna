import { Component, OnInit } from '@angular/core';
import {SubscribersService} from '../../shared/services/subscribers.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-gestionuser',
  templateUrl: './gestionuser.component.html',
  styleUrls: ['./gestionuser.component.scss']
})
export class GestionuserComponent implements OnInit {
  myData: any[] = [];
  constructor(private subservice: SubscribersService) { }

  ngOnInit() { this.listsub();
  }

  listsub(){
this.subservice.getsubscribers().pipe(first()).subscribe(data=>{
this.myData = data as string [];
  }, err => {
    console.log(err);
  }) } 

}
