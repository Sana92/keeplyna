import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './User-routing.module';
import { GestionuserComponent } from './gestionuser.component';

@NgModule({
    imports: [CommonModule, UserRoutingModule],
    declarations: [GestionuserComponent]
})
export class GestionuserModuleModule {}
