import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GestionuserComponent } from './gestionuser.component';

const routes: Routes = [
    {
        path: '', component: GestionuserComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserRoutingModule {
}
