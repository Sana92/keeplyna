import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../router.animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AuthentificationService} from '../shared/services/authentification.service';
import {first} from 'rxjs/operators';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    private returnUrl: string;
    error = '';
    isSubmitted  =  false;
    isResponseInvalid :boolean;
    ResponseInvalid:string ;

    constructor(private _auth: AuthentificationService,
                private formBuilder: FormBuilder,
                private router: Router) {
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }
    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }


    onSubmit() {
      this.isSubmitted = true;
      // stop here if form is invalid
      if (this.loginForm.invalid) {
          return;
      }

        this._auth.loginUser(this.f.username.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                        if(data.result == "Success"){
                        localStorage.setItem('token', data.token);
                        localStorage.setItem('role', data.role[0]);
                        localStorage.setItem('username', data.username);
                        this.isResponseInvalid = false;
                        localStorage.setItem('isLoggedin', 'true');

                        this.router.navigate(['/dashboard']);
                        }else{
                             this.ResponseInvalid = data.result;
                             this.isResponseInvalid = true;

                        }
                },
                error => {
                    this.error = error;
                });
    }
    onLoggedin() {
      //  localStorage.setItem('isLoggedin', 'true');
    }
}
